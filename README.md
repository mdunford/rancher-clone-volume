# clone a rancher nfs volume

## How it should work

- source volume must exist
- destination volume must not exist
- it fires up a temp stack to copy the source to the destination
- remove the temp stack

## example

```
cori05 [1152]$ rancher volume
ID          NAME                            STATE      DRIVER        DETAIL
1v3103559   app-vol1.spintst3-first-stack   detached   rancher-nfs

cori05 [1153]$ ./clone-volume.sh app-vol1.spintst3-first-stack app-vol2.spintst3-first-stack
1v3106528
INFO[0000] Creating stack dunford-clonevolume
INFO[0000] [app]: Creating
INFO[0000] Creating service app
INFO[0002] [app]: Created
INFO[0002] [app]: Starting
1s11580
INFO[0007] [app]: Started

cori05 [1156]$ rancher up --upgrade -s spintst3-first-stack -d -f both.yml
INFO[0001] Creating volume template app-vol1.spintst3-first-stack
INFO[0001] Creating volume template app-vol2.spintst3-first-stack
INFO[0001] [app]: Creating
INFO[0001] Creating service app
INFO[0002] [app]: Created
INFO[0002] Existing volume template found for app-vol1.spintst3-first-stack
INFO[0002] Existing volume template found for app-vol2.spintst3-first-stack
INFO[0002] [app]: Starting
1s11581
INFO[0008] [app]: Started

cori05 [1157]$ rancher exec spintst3-first-stack/app ls -l /vol1
total 5148
-rw-r--r-- 1 root root       6 Apr 25 19:41 hi
-rw-r--r-- 1 root root 5242880 Apr 25 19:41 some-data

cori05 [1158]$ rancher exec spintst3-first-stack/app ls -l /vol2
total 4
drwxr-xr-x 2 root root 4096 Apr 25 19:41 src

cori05 [1159]$ rancher exec spintst3-first-stack/app ls -l /vol2/src
total 5148
-rw-r--r-- 1 root root       6 Apr 25 19:41 hi
-rw-r--r-- 1 root root 5242880 Apr 25 19:41 some-data

cori05 [1182]$ rancher volume
ID          NAME                            STATE     DRIVER        DETAIL
1v3103559   app-vol1.spintst3-first-stack   active    rancher-nfs
1v3106528   app-vol2.spintst3-first-stack   active    rancher-nfs
```
