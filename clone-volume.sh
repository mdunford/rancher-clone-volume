#!/bin/bash

SRC=$1
DST=$2
if [ -z "$SRC" ] || [ -z "$DST" ]; then
	echo usage: $0 source-volume destination-volume
	exit 1
fi

module load spin

function does_vol_exist {
	local RETVAL VOL
	VOL=$1
	RETVAL=$(rancher volume ls | sed 1d | awk -v VOL=$VOL '$2 == VOL {print "YES"}')
	if [ "x$RETVAL" = xYES ]; then
		return 0
	fi
	return 1
}

function does_stack_exist {
	local RETVAL STACK
	STACK=$1
	RETVAL=$(rancher stack ls | sed 1d | awk -v STACK=$STACK '$2 == STACk {print "YES"}')
	if [ "x$RETVAL" = xYES ]; then
		return 0
	fi
	return 1
}

# check if src volume exists, exit if it doesn't
does_vol_exist $SRC
if [ $? = 1 ]; then
	echo source volume $SRC doesnt exist
	exit 1
fi

# check if dst volume exists, exit if it does
does_vol_exist $DST
if [ $? = 0 ]; then
	echo destination volume $DST already exists
	exit 1
fi

# create dst volume
rancher volume create --driver rancher-nfs $DST

# verify creation
does_vol_exist $DST
if [ $? = 1 ]; then
	echo destination volume $DST wasnt properly created
	exit 1
fi

# generate clone stack
TMPF=$(mktemp)
trap "rm -f $TMPF" EXIT
cat >$TMPF <<EOF
version: '2'
services:
  app:
    image: alpine
    cap_drop:
    - ALL
    volumes:
    - $SRC:/src
    - $DST:/dst
    command: "cp -a /src/ /dst/"
    labels:
      io.rancher.container.start_once: 'true'
EOF

STACK=${USER}-clonevolume
does_stack_exist $STACK
if [ $? = 0 ]; then
	echo stack $STACK already exists
	exit 1
fi
# launch stack and wait for completion
rancher --wait-state started-once up -d -f $TMPF -s $STACK
# clean up
rancher rm --type stack $STACK
